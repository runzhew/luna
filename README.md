## Luna

A game based on SDL and a beautiful story.


## ScreenShot

![screen1](https://github.com/run/luna/blob/newest/src/images/screen.png)


![screen2](https://github.com/run/luna/blob/newest/src/images/screen2.png)
