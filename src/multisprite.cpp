#include <iostream>
#include <cmath>
#include "multisprite.h"
#include "gamedata.h"
#include "frameFactory.h"

void MultiSprite::advanceFrame(Uint32 ticks)
{
    timeSinceLastFrame += ticks;
    if (timeSinceLastFrame > frameInterval) {
        currentFrame = (currentFrame+1) % numberOfFrames;
        timeSinceLastFrame = 0;
    }
}

static CollisionStrategy* getStrategy(const string& name) {
    std::string sName = Gamedata::getInstance().getXmlStr(name+"Strategy");
    if (sName == "midpoint") return new MidPointCollisionStrategy;
    if (sName == "rectangular") return new RectangularCollisionStrategy;
    if (sName == "perpixel") return new PerPixelCollisionStrategy;
    throw std::string("No strategy");
}

MultiSprite::MultiSprite( const std::string& name) :
    Drawable(name,
             Vector2f(Gamedata::getInstance().getXmlInt(name+"X"),
                      Gamedata::getInstance().getXmlInt(name+"Y")),
             Vector2f(Gamedata::getInstance().getXmlInt(name+"SpeedX"),
                      Gamedata::getInstance().getXmlInt(name+"SpeedY"))
            ),
    frames( FrameFactory::getInstance().getFrames(name) ),
    worldWidth(Gamedata::getInstance().getXmlInt("worldWidth")),
    worldHeight(Gamedata::getInstance().getXmlInt("worldHeight")),

    dt(0),
    currentFrame(0),
    numberOfFrames( Gamedata::getInstance().getXmlInt(name+"Frames") ),
    frameInterval( Gamedata::getInstance().getXmlInt(name+"FrameInterval") ),
    timeSinceLastFrame( 0 ),
    frameWidth(frames[0]->getWidth()),
    frameHeight(frames[0]->getHeight()),
    STATUS(0),
    strategy(getStrategy(name))
{ }

MultiSprite::MultiSprite(const std::string& name,
                         const Vector2f& pos, const Vector2f& vel,
                         const std::vector<Frame*>& fms) :
    Drawable(name, pos, vel),
    frames(fms),
    worldWidth(Gamedata::getInstance().getXmlInt("worldWidth")),
    worldHeight(Gamedata::getInstance().getXmlInt("worldHeight")),

    dt(0),
    currentFrame(0),
    numberOfFrames( Gamedata::getInstance().getXmlInt(name+"Frames") ),
    frameInterval( Gamedata::getInstance().getXmlInt(name+"FrameInterval") ),
    timeSinceLastFrame( 0 ),
    frameWidth(fms[0]->getWidth()),
    frameHeight(fms[0]->getHeight()),
    STATUS(0),
    strategy(getStrategy(name))
{ }

MultiSprite::MultiSprite(const MultiSprite& s) :
    Drawable(s.getName(), s.getPosition(), s.getVelocity()),
    frames(s.frames),
    worldWidth( s.worldWidth ),
    worldHeight( s.worldHeight ),
    dt(s.dt),
    currentFrame(s.currentFrame),
    numberOfFrames( s.numberOfFrames ),
    frameInterval( s.frameInterval ),
    timeSinceLastFrame( s.timeSinceLastFrame ),
    frameWidth( s.frameWidth ),
    frameHeight( s.frameHeight ),
    STATUS(0),
    strategy(s.strategy)
{ }

// this operator has bug, I wirte it just to shut up the complier.
// I will fix this bug after finishing the proj.
MultiSprite& MultiSprite::operator = (const MultiSprite& s)
{
    strategy = s.strategy;
    return *this;
}

void MultiSprite::draw() const
{
    Uint32 x = static_cast<Uint32>(X());
    Uint32 y = static_cast<Uint32>(Y());
    frames[currentFrame]->draw(x, y);
}

void MultiSprite::setStatus(int s)
{
    STATUS = s;
}

void MultiSprite::update(Uint32 ticks)
{
    advanceFrame(ticks);

    switch(STATUS) {
    case 0:
    case 1:
	{
	    Vector2f incr1 = getVelocity() * static_cast<float>(ticks) * 0.001;
            incr1[1] = 0;
            setPosition(getPosition() + incr1);
            break;
	}
    case 2:
	{
	    float t = static_cast<float>(ticks) * 0.001;
	    float g = 420;
	    Vector2f v0 = getVelocity();
	    //Vector2f bug;
	    Vector2f pos;
	    //std::cout << "v0[0] = " << v0[0] << "  " << "v0[1] = " << v0[1] << std::endl;
	    Vector2f v = v0; // I want the X speed.
	    Vector2f incr2;
	    incr2[0] = v0[0] * t;
	    incr2[1] = v0[1] * t + 0.5 * g * t * t;
	    //std::cout << "incr2[1] = " << incr2[1] << std::endl;
	    
	    v[1] = v0[1] + g * t;

	    //std::cout << "before setVelocity " << v[0] << ", " << v[1] << std::endl;
	    setVelocity(v);
	    //bug	 = getVelocity();
	    //std::cout << "after setVelocity  " << bug[0] << ", " << bug[1] << std::endl << std::endl;

	    pos = getPosition() + incr2;

	    if (pos[1] == 330) {
	        setVelocitytoZero();
		setPosition(pos);
	    }
	    else {
		setPosition(pos);
	    }
            break;
	}
    case 4:
	{
	    setVelocitytoZero();
	}
    default:
       break;
    }

    /*if ( Y() < 0) {
        velocityY( abs( velocityY() ) );
    }
    if ( Y() > worldHeight-frameHeight) {
        velocityY( -abs( velocityY() ) );
    }

    if ( X() < 0) {
        velocityX( abs( velocityX() ) );

    }
    if ( X() > worldWidth-frameWidth) {
        velocityX( -abs( velocityX() ) );
    }*/

}

int MultiSprite::getworldWidth() const
{
    return worldWidth;
}

int MultiSprite::getframeWidth() const
{
    return frameWidth;
}

bool MultiSprite::collidedWith(const MultiSprite *obj)
{
    if (strategy->execute(*this, *obj)) {
	return true;
    }
    else {
	return false;
    }
}

