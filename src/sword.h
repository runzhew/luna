#ifndef __SWORD_H__
#define __SWORD_H__

#include <list>
#include "weapon.h"
#include "collisionStrategy.h"

class Sword {
public:
    Sword(const std::string&);
    Sword(const Sword&);
    ~Sword();
    void draw() const;
    void update(Uint32 ticks);
    void shoot(const Vector2f& pos, const Vector2f& vel);

    unsigned int swordCount() const {return swordList.size();}
    unsigned int freeCount() const {return freeList.size();}
    bool shooting() const {return swordList.empty();}
    bool collidedWith(const MultiSprite *obj);

private:
    std::string name;
    CollisionStrategy * strategy;
    //SDL_Surface *swordSurface;
    std::vector<Frame *> swordFrame;
    float frameInterval;
    float timeSinceLastFrame;
    std::list<Weapon> swordList;
    std::list<Weapon> freeList;

    Sword& operator = (const Sword&);
};

#endif
