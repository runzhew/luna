#include <iostream>
#include <cmath>
#include "ioManager.h"
#include "gamedata.h"
#include "frameFactory.h"
#include "sword.h"

static CollisionStrategy* getStrategy(const string& name) {
    std::string sName = Gamedata::getInstance().getXmlStr(name+"Strategy");
    if (sName == "midpoint") return new MidPointCollisionStrategy;
    if (sName == "rectangular") return new RectangularCollisionStrategy;
    if (sName == "perpixel") return new PerPixelCollisionStrategy;
    throw std::string("No strategy");
}

Sword::~Sword() {
    //SDL_FreeSurface(swordSurface);
    //delete swordSurface;
    delete strategy;
}

Sword::Sword(const std::string& n) :
    name(n),
    strategy(getStrategy(name)),
    //swordSurface(IOManager::getInstance().loadAndSet(Gamedata::getInstance().getXmlStr(name+"File"), true)),
    swordFrame(FrameFactory::getInstance().getFrames(name)),
    frameInterval(Gamedata::getInstance().getXmlInt(name+"SwordInterval")),
    timeSinceLastFrame(0),
    swordList(),
    freeList()
{}

Sword::Sword(const Sword &b):
    name(b.name),
    strategy(b.strategy),
    //swordSurface(b.swordSurface),
    swordFrame(b.swordFrame),
    frameInterval(b.frameInterval),
    timeSinceLastFrame(b.timeSinceLastFrame),
    swordList(b.swordList),
    freeList(b.freeList)
{}

bool Sword::collidedWith(const MultiSprite* obj) {
    std::list<Weapon>::iterator ptr = swordList.begin();
    while (ptr != swordList.end()) {
	if (strategy->execute(*ptr, *obj)){
	    freeList.push_back(*ptr);
	    ptr = swordList.erase(ptr);
	    return true;
	}
	++ptr;
    }
    return false;
}

void Sword::shoot(const Vector2f& position, const Vector2f& velocity) {
    if (timeSinceLastFrame > frameInterval) {
	if (freeList.empty()) {
	    Weapon s(name, position, velocity, swordFrame);
	    swordList.push_back(s);
	}
	else {
	    Weapon s = freeList.front();
	    freeList.pop_front();
	    s.reset();
	    s.setVelocity(velocity);
	    s.setPosition(position);
	    swordList.push_back(s);
	}
	timeSinceLastFrame = 0;
    }
}

void Sword::draw() const{
    std::list<Weapon>::const_iterator ptr = swordList.begin();
    while (ptr != swordList.end()) {
	ptr->draw();
	++ptr;
    }
}

void Sword::update(Uint32 ticks) {
    timeSinceLastFrame += ticks;
    std::list<Weapon>::iterator ptr = swordList.begin();
    while (ptr != swordList.end()) {
	ptr->update(ticks);
	if (ptr->goneTooFar()) {
	    freeList.push_back(*ptr);
	    ptr = swordList.erase(ptr);
	}
	else {
	    ++ptr;
	}
    }
}
