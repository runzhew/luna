#include <iostream>
#include <cmath>
#include "sprite.h"
#include "gamedata.h"
#include "frameFactory.h"

Sprite::Sprite(const std::string& name) :
    Drawable(name,
             Vector2f(Gamedata::getInstance().getXmlInt(name+"X"),
                      Gamedata::getInstance().getXmlInt(name+"Y")),
             Vector2f(Gamedata::getInstance().getXmlInt(name+"SpeedX"),
                      Gamedata::getInstance().getXmlInt(name+"SpeedY"))
            ),
    frame( FrameFactory::getInstance().getFrame(name) ),
    frameWidth(frame->getWidth()),
    frameHeight(frame->getHeight()),
    worldWidth(Gamedata::getInstance().getXmlInt("worldWidth")),
    worldHeight(Gamedata::getInstance().getXmlInt("worldHeight")),
    zoom()
{
    
    int max = Gamedata::getInstance().getXmlInt("RAND_MAX");
    int min = Gamedata::getInstance().getXmlInt("RAND_MIN");

    Vector2f  vel(
        (rand() % (max  - min)),
        (rand() % (max  - min))
    );
    Vector2f  pos((rand() % (max  - min) + min), 0);

    zoom = (rand() % 10) * 0.1 + 1;

    setVelocity(vel);
    setPosition(pos);
    
}

// This constructor is just for the Porject5 requirement
// which introduce the AI. In order to finish porj 5,
// I wrote these ugly code.... after I submit my assigment 
// I will do the clean up ;-)
// 
// However, this constructor is used in SmartSprite
//
Sprite::Sprite(const string& name, const Vector2f& pos, 
               const Vector2f& vel):
  Drawable(name, pos, vel), 
  frame( FrameFactory::getInstance().getFrame( name ) ),
  frameWidth(frame->getWidth()),
  frameHeight(frame->getHeight()),
  worldWidth(Gamedata::getInstance().getXmlInt("worldWidth")),
  worldHeight(Gamedata::getInstance().getXmlInt("worldHeight")),
  zoom()
{ 

    int max = Gamedata::getInstance().getXmlInt("RAND_MAX");
    int min = Gamedata::getInstance().getXmlInt("RAND_MIN");

    Vector2f  v(
        (rand() % (max  - min)),
        (rand() % (max  - min))
    );
    Vector2f  p((rand() % (max  - min) + min), 0);

    zoom = (rand() % 10) * 0.1 + 1;

    setVelocity(v);
    setPosition(p);
}

Sprite::Sprite(const Sprite& s) :
    Drawable(s.getName(), s.getPosition(), s.getVelocity()),
    frame(s.frame),
    frameWidth(s.getFrame()->getWidth()),
    frameHeight(s.getFrame()->getHeight()),
    worldWidth(Gamedata::getInstance().getXmlInt("worldWidth")),
    worldHeight(Gamedata::getInstance().getXmlInt("worldHeight")),
    zoom()
{ }

// for explosion
Sprite::Sprite(const string& name, const Vector2f& pos, const Vector2f& vel, const Frame*& fm):
    Drawable(name, pos, vel),
    frame(fm),
    frameWidth(frame->getWidth()),
    frameHeight(frame->getHeight()),
    worldWidth(Gamedata::getInstance().getXmlInt("worldWidth")),
    worldHeight(Gamedata::getInstance().getXmlInt("worldHeight")),
    zoom()
{}

Sprite& Sprite::operator=(const Sprite& rhs)
{
    setName( rhs.getName() );
    setPosition(rhs.getPosition());
    setVelocity(rhs.getVelocity());
    frame = rhs.frame;
    frameWidth = rhs.frameWidth;
    frameHeight = rhs.frameHeight;
    worldWidth = rhs.worldWidth;
    worldHeight = rhs.worldHeight;
    return *this;
}

void Sprite::draw() const
{
    Uint32 x = static_cast<Uint32>(X());
    Uint32 y = static_cast<Uint32>(Y());
    frame->draw(x, y);
}

int Sprite::getDistance(const Sprite *obj) const
{
    return hypot(X()-obj->X(), Y()-obj->Y());
}

void Sprite::update(Uint32 ticks)
{
    Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
    setPosition(getPosition() + incr);

    if ( Y() < 0) {
        velocityY( abs( velocityY() ) );
    }
    if ( Y() > worldHeight-frameHeight) {
        velocityY( -abs( velocityY() ) );
    }

    if ( X() < 0) {
        velocityX( abs( velocityX() ) );
    }
    if ( X() > worldWidth-frameWidth) {
        velocityX( -abs( velocityX() ) );
    }
}
