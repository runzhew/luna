#include <iostream>
#include <string>
#include <algorithm>
#include "sprite.h"
#include "multisprite.h"
#include "gamedata.h"
#include "manager.h"
#include "hud.h"
#include "sound.h"
#include "explodingSprite.h"
#include "health.h"
#include "smartSprite.h"

Manager::~Manager()
{
    // These deletions eliminate "definitely lost" and
    // "still reachable"s in Valgrind.
    for (unsigned i = 0; i < sprites.size() - 1; ++i) {
        //std::cout << "delete NO. " << i << "  sprites"<< std::endl;
        delete sprites[i];
    }

    delete explodingEnemy;
    delete enemy;
    delete world;
    delete life;
}

Manager::Manager() :
    env( SDL_putenv(const_cast<char*>("SDL_VIDEO_CENTERED=center")) ),
    io( IOManager::getInstance() ),
    clock( Clock::getInstance() ),
    screen( io.getScreen() ),
    world(NULL),
    world1("back1", Gamedata::getInstance().getXmlInt("back1Factor") ),
    viewport( Viewport::getInstance() ),
    sprites(),
    currentSprite(0),
    sprite_n(Gamedata::getInstance().getXmlInt("Sprit_N")),
    F1_help(false),
    player(),
    explodingPlayer(&player),
    thePlayer(&player),
    yue("yue"),
    FPS(Gamedata::getInstance().getXmlInt("FRAMES_PER_SECOND")),
    hud(),
    shootingSword("sword"),
    enemy(new MultiSprite("enemy")), // REMEMBER to free this
    explodingEnemy(new ExplodingSprite(*enemy)), // remember to free this
    theEnemy(enemy),
    life(NULL),
    lifeValue(200)
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        throw string("Unable to initialize SDL: ");
    }
    atexit(SDL_Quit);

    // set the default background
    world = new World("back3", Gamedata::getInstance().getXmlInt("back3Factor") );
    life = new  Health(lifeValue);

    sprites.push_back(new MultiSprite("sun"));
    Vector2f pos = sprites[0]->getPosition();
    Sprite * tmp = static_cast<Sprite*>( sprites[0] );

    for (int i = 1; i < sprite_n; i++) {
        sprites.push_back( new SmartSprite("goldcoin30", pos, *tmp ));
    }

    sprites.push_back( player.getcurrent() );
    currentSprite = sprites.size() - 1;

    viewport.setObjectToTrack(sprites[currentSprite]);

    enemy->setStatus(ENEMY);
    yue.setStatus(PAUSE);
}

int explodingTimes = 0;

void Manager::draw() const
{
    world1.draw();
    world->draw();
    for (unsigned i = 0; i < sprites.size(); ++i) {
        sprites[i]->draw();
    }
    /*for (unsigned i = sprites.size() / 2; i < sprites.size(); ++i) {
        sprites[i]->draw();
    }*/

    if (F1_help) {
        hud.drawHud();
        io.printMessageValueAt("Seconds: ", clock.getSeconds(), 10, 20);
        io.printMessageValueAt("fps: ", clock.getFps(), 10, 40);
        io.printMessageValueAt("Sword in use: ", shootingSword.swordCount(), 10, 60);
        io.printMessageValueAt("Sword in memory pooling: ", shootingSword.freeCount(), 10, 80);
        io.printMessageValueAt("Explosion : ", explodingTimes, 10, 100);
        string message = "Tracking: "+ sprites[currentSprite]->getName();
        io.printMessageAt(message, 10, 120);
    }
    
    shootingSword.draw();
    theEnemy->draw();
    life->draw();
    //yue.draw();
    SDL_Flip(screen);
}

void Manager::update()
{
    ++clock;
    Uint32 ticks = clock.getElapsedTicks();
    // update the player
    sprites[sprites.size() - 1] = player.getcurrent();

    for (unsigned int i = 0; i < sprites.size(); ++i) {
        sprites[i]->update(ticks);
    }
    //std::cout <<"sprites[10] address  "<<sprites.size() <<sprites[10] <<std::endl;
    world1.update();
    world->update();
    viewport.update(); // always update viewport last
    shootingSword.update(ticks);
    theEnemy->update(ticks);
    //yue.update(ticks);


    if (lifeValue != DEFAULT_LIFE) {
	life->reset(lifeValue);
	lifeValue = DEFAULT_LIFE;
    }

    if (ticks < (unsigned)(1000 / FPS))
        SDL_Delay((1000 / FPS) - ticks);
}


void Manager::play()
{
    SDL_Event event;
    SDLSound  sound;

    bool done = false;
    bool keyCatch = false;
    bool jumpping = false;
    bool walking = false;
    bool once = true; // change the background only once
    bool enemyExploding = false;

    while ( not done ) {
	walking = false;

	if (life->getCurrentLife() < 0) 
	    break;

	if (once) {
	    theEnemy->setPosition(1300, 330);
	    Vector2f tmp = thePlayer->getcurrent()->getPosition();
	    if (tmp[0] <= 0) {
		tmp[0] = 0;
		thePlayer->getcurrent()->setPosition(tmp);
	    }
		
	}
	else {
	    theEnemy->setPosition(850, 330);
	    //yue.setPosition(1300, 330);
	    Vector2f tmp = thePlayer->getcurrent()->getPosition();
	    if (tmp[0] > 1200) {
		tmp[0] = 1200;
		thePlayer->getcurrent()->setPosition(tmp);
	    }
	}

	// change the background
	Vector2f p = thePlayer->getcurrent()->getPosition();
	if (once && p[0] > 1200) {
	    delete world;
	    world = new World("back2", Gamedata::getInstance().getXmlInt("back2Factor") );
	    p[0] = 50;
	    thePlayer->getcurrent()->setPosition(p);
	    once = false;
	}

	// The new strategy is when the thePlayer->collides with the Enemy
	// the thePlayer->should explode
	if (thePlayer->getcurrent()->collidedWith(theEnemy)) {
	    const Vector2f &tmp = thePlayer->getcurrent()->getPosition();
	    const Vector2f &vel = thePlayer->getcurrent()->getVelocity();
	    life->update();
	    thePlayer->getcurrent()->setVelocity(-vel);
	    thePlayer->getcurrent()->setStatus(JUMP);
	    thePlayer->getcurrent()->setPosition(tmp);
	    viewport.setObjectToTrack(sprites[currentSprite]);
	}

	for (unsigned int i = 1; i < sprites.size() - 2; i++) {
	    MultiSprite * tmp = (MultiSprite*) sprites[i];
	    if (thePlayer->getcurrent()->collidedWith(tmp)) {
		life->update();
	    }
	}

	if (thePlayer->onGround() == true) {
	    jumpping = false;
	}

	if (enemyExploding) {
	    if (static_cast<ExplodingSprite*>(theEnemy)->chunkCount() == 0) {
		enemyExploding = false;
		break;
		theEnemy = enemy;
	    }
	}

        SDL_PollEvent(&event);
        Uint8 *keystate = SDL_GetKeyState(NULL);
        if (event.type ==  SDL_QUIT) {
            done = true;
            break;
        }
        if (event.type == SDL_KEYUP) {
            keyCatch = false;
	    // if free any key , the sprite become a static sprite
            if (!keystate[SDLK_RIGHT] && !keystate[SDLK_LEFT] && !jumpping) {
	        const Vector2f &pos = thePlayer->getcurrent()->getPosition();
		if (thePlayer->getcurrent() == thePlayer->getleft())
		    thePlayer->setcurrent(thePlayer->getpauseLeft());
		else if (thePlayer->getcurrent() == thePlayer->getright())
		    thePlayer->setcurrent(thePlayer->getpauseRight());

		thePlayer->getcurrent()->setPosition(pos);
	        thePlayer->getcurrent()->setStatus(PAUSE);
            }
        }

        if (event.type == SDL_KEYDOWN) {
            if (keystate[SDLK_ESCAPE] || keystate[SDLK_q]) {
                done = true;
                break;
            }
            
	    if (keystate[SDLK_F1] && !keyCatch) {
                keyCatch = true;
                F1_help = F1_help ? false : true;
            }

            if (keystate[SDLK_LEFT] && !keyCatch && !jumpping) {
                if (thePlayer->getcurrent() != thePlayer->getleft()) {
                    const Vector2f& tmp = thePlayer->getcurrent()->getPosition();
                    thePlayer->setcurrent(thePlayer->getleft());
                    thePlayer->getcurrent()->setPosition(tmp);
                }
                const Vector2f& vel = thePlayer->getcurrent()->getVelocity();
		if (vel[0] > 0)
		    thePlayer->getcurrent()->setVelocity(-vel);
                //thePlayer->setSpeed(-200, 0);
                //sprites[sprites.size() - 1] = thePlayer->getcurrent();
		thePlayer->getcurrent()->setStatus(LEFT);
                viewport.setObjectToTrack(sprites[currentSprite]);
		walking = true;
            }

            if (keystate[SDLK_RIGHT] && !keyCatch && !jumpping) {
                const Vector2f& tmp = thePlayer->getcurrent()->getPosition();
                if (thePlayer->getcurrent() != thePlayer->getright()) {
                    thePlayer->setcurrent(thePlayer->getright());
                    thePlayer->getcurrent()->setPosition(tmp);
                }
                const Vector2f& vel = thePlayer->getcurrent()->getVelocity();
		if (vel[0] < 0)
		    thePlayer->getcurrent()->setVelocity(-vel);
                //thePlayer->setSpeed(200, 0);
                //sprites[sprites.size() - 1] = thePlayer->getcurrent();
		thePlayer->getcurrent()->setStatus(RIGHT);
                viewport.setObjectToTrack(sprites[currentSprite]);
		walking = true;
            }

            if (keystate[SDLK_SPACE] && !keyCatch && !jumpping) {
		MultiSprite * tmp = thePlayer->getcurrent();
                Vector2f v = tmp->getVelocity();
		v[1] = -300;
                Vector2f pos = tmp->getPosition();
		if (v[0] > 0) {
		    thePlayer->setcurrent(thePlayer->getjumpRight());
		}
		else if (v[0] < 0) {
		    thePlayer->setcurrent(thePlayer->getjumpLeft());
		}
		thePlayer->getcurrent()->setVelocity(v);
		thePlayer->getcurrent()->setPosition(pos);
		viewport.setObjectToTrack(sprites[currentSprite]);
		thePlayer->getcurrent()->setStatus(JUMP);
		jumpping = true;
            }

	    if (keystate[SDLK_f] && !keyCatch && !jumpping && walking) {
		
		sound[3];
		// the sword's speed is double than sprite 
		Vector2f v = thePlayer->getcurrent()->getVelocity() * 2;
		Vector2f p = thePlayer->getcurrent()->getPosition();
		p[0] += 30;
		p[1] += 30;
		
		shootingSword.shoot(p, v);
		if (shootingSword.collidedWith(theEnemy)) {
		    sound[5];
		    if (!enemyExploding) {
			enemyExploding = true;
			static_cast<ExplodingSprite*>(explodingEnemy)->
			    reset(theEnemy->getPosition());
			theEnemy = explodingEnemy;
			explodingTimes++;
		    }
		}
	    }

        }

        draw();
        update();
    }
}
