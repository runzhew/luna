#include <vector>
#include <SDL.h>
#include "ioManager.h"
#include "clock.h"
#include "world.h"
#include "viewport.h"
#include "player.h"
#include "hud.h"
#include "sword.h"
#include "health.h"

#define DEFAULT_LIFE  200

class Manager
{
public:
    Manager ();
    ~Manager ();
    void play();
    void pause() { clock.pause(); }
    void unpause() { clock.unpause(); }
    void setLifeValue(int val) {lifeValue = val;}

private:
    const bool env;
    const IOManager& io;
    Clock& clock;

    SDL_Surface * const screen;
    World *  world;
    World world1;
    Viewport& viewport;

    std::vector<Drawable*> sprites;
    int currentSprite;

    int sprite_n;
    bool F1_help;
    Player player;
    Player * explodingPlayer;
    Player * thePlayer;
    MultiSprite yue;
    int FPS;
    Hud hud;
    Sword shootingSword;
    MultiSprite *enemy;
    MultiSprite *explodingEnemy;
    MultiSprite *theEnemy;
    Health*  life;
    int lifeValue;

    void draw() const;
    void update();
    void playerJump();

    Manager(const Manager&);
    Manager& operator=(const Manager&);
};
