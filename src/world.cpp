#include <iostream>
#include "world.h"
#include "frameFactory.h"

World::World(const std::string& name, int fact) :
    io( IOManager::getInstance() ),
    frame( FrameFactory::getInstance().getFrame(name) ),
    factor(fact),
    frameWidth( frame->getWidth() ),
    worldWidth( Gamedata::getInstance().getXmlInt("worldWidth") ),
    viewX(0.0), viewY(0.0),
    posX(Gamedata::getInstance().getXmlFloat(name+"PosX")),
    posY(Gamedata::getInstance().getXmlFloat(name+"PosY")),
    view(Viewport::getInstance())
{}

void World::update()
{
    viewX = static_cast<int>(view.X() / factor) % frameWidth;
    viewY = view.Y();
}

void World::draw() const
{
    frame->draw(viewX, viewY, posX, posY);
    frame->draw(0, viewY, frameWidth-viewX, 0);
}

