#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "multisprite.h"

class Player
{
public:
    Player();
    Player(const Player& );
    Player& operator = (const Player &);
    MultiSprite* getcurrent() const;
    MultiSprite * getleft();
    MultiSprite * getright();
    MultiSprite * getjumpLeft();
    MultiSprite * getjumpRight();
    MultiSprite * getpauseLeft();
    MultiSprite * getpauseRight();
    void stop();
    void setcurrent(MultiSprite *);
    void setSpeed(float, float);
    bool onGround();

private:
    MultiSprite right;
    MultiSprite left;
    MultiSprite jump_left;
    MultiSprite jump_right;
    MultiSprite pause_left;
    MultiSprite pause_right;
    MultiSprite * current;
};

#endif
