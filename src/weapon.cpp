#include <iostream>
#include <cmath>
#include "weapon.h"
#include "collisionStrategy.h"

void Weapon::update(Uint32 ticks) {
    Vector2f pos = getPosition();
    MultiSprite::update(ticks);
    distance += (hypot(X() - pos[0], Y()-pos[1]));
    if (distance > maxDistance) tooFar = true;
}
