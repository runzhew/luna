#include <cmath>
#include <iostream>
#include <string>
#include <sstream>
#include "menuManager.h"
#include "manager.h"

MenuManager::MenuManager() :
    env( SDL_putenv(const_cast<char*>("SDL_VIDEO_CENTERED=center")) ),
    screen( IOManager::getInstance().getScreen() ),
    clock( Clock::getInstance() ),
    backColor(),
    menu(),
    lifeValue(200)
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        throw string("Unable to initialize SDL: ");
    }
    backColor.r = Gamedata::getInstance().getXmlInt("backRed");
    backColor.g = Gamedata::getInstance().getXmlInt("backGreen");
    backColor.b = Gamedata::getInstance().getXmlInt("backBlue");
    atexit(SDL_Quit);
}

void MenuManager::drawBackground() const
{
    SDL_FillRect( screen, NULL,
                  SDL_MapRGB(screen->format, backColor.r, backColor.g, backColor.b) );
    SDL_Rect dest = {0, 0, 0, 0};
    SDL_SetAlpha( screen, SDL_SRCALPHA | SDL_RLEACCEL, 50 );
    SDL_BlitSurface( screen, NULL, screen, &dest );
}

void MenuManager::getParameters()
{
    IOManager& io = IOManager::getInstance().getInstance();
    SDL_Event event;
    bool done = false;
    bool nameDone = false;
    const string msg("How many Health Value do you want? : ");
    io.clearString();
    while ( not done ) {
        Uint8 *keystate = SDL_GetKeyState(NULL);
        if ( SDL_PollEvent(&event) )
            switch (event.type) {
            case SDL_KEYDOWN: {
                if (keystate[SDLK_ESCAPE] || keystate[SDLK_q]) {
                    done = true;
                }
                if (keystate[SDLK_RETURN]) {
                    nameDone = true;
                }
                io.buildString(event);
            }
            }
        drawBackground();
        io.printStringAfterMessage(msg, 20, 120);
        if ( nameDone ) {
            std::string number = io.getString();
            std::stringstream strm;
            strm << number;
            strm >> lifeValue;
            strm.clear(); // clear error flags
            strm.str(std::string()); // clear contents
            strm << "Okay -- Your Health Value is " << lifeValue << "";
            io.printMessageAt(strm.str(), 20, 160);
            SDL_Flip(screen);
            SDL_Delay(500);
            done = true;
        }
        if ( !done ) {
            SDL_Flip(screen);
        }
    }
}

void MenuManager::printNewGame()
{
    IOManager& io = IOManager::getInstance().getInstance();
    drawBackground();
    const string msg1("Okay, New Game is ready!!! ");
    io.printStringAfterMessage(msg1, 20, 150);
    const string msg2("Please click the 'Play the Game' ");
    io.printStringAfterMessage(msg2, 20, 220);
    SDL_Flip(screen);
    SDL_Delay(2000);
}

void MenuManager::printHelp()
{
    IOManager& io = IOManager::getInstance().getInstance();
    drawBackground();
    const string msg1("Press Left/Right key to move");
    io.printStringAfterMessage(msg1, 20, 150);
    const string msg2("Press Space key to jump ");
    io.printStringAfterMessage(msg2, 20, 220);
    const string msg3("Press f key to fire ");
    io.printStringAfterMessage(msg3, 20, 290);
    SDL_Flip(screen);
    SDL_Delay(2000);
}

void MenuManager::play()
{
    bool keyCatch = false; // To get only 1 key per keydown
    SDL_Event event;
    bool done = false;
    // Here, we need to create an instance of the Manager,
    // the one that manages the game not the menu:
    Manager* manager = new Manager();

    while ( not done ) {

        drawBackground();
        menu.draw();
        SDL_Flip(screen);

        SDL_PollEvent(&event);
        if (event.type ==  SDL_QUIT) {
            break;
        }
        if(event.type == SDL_KEYDOWN) {
            switch ( event.key.keysym.sym ) {
            case SDLK_ESCAPE :
            case SDLK_q : {
                done = true;
                break;
            }
            case SDLK_RETURN : {
                if ( !keyCatch ) {
                    menu.lightOn();
                    if ( menu.getIconClicked() == "Play the Game" ) {
                        // Here is where we call the play() function in Manager;
                        // but first, unpause the game:
                        manager->unpause();
                        manager->play();
                        manager->pause();
                    }
                    if ( menu.getIconClicked() == "New Game" ) {
			delete manager;
			manager = new Manager();
			printNewGame();
                    }
                    if ( menu.getIconClicked() == "Setting Parameters" ) {
			getParameters();
                        // After we get the number, we must set it to Manager:
                        manager->setLifeValue( lifeValue );
                    }
                    if ( menu.getIconClicked() == "Help" ) {
                        // Here is where we explain how to play the game"
			printHelp();
                    }
                    if ( menu.getIconClicked() == "Exit" ) {
                        drawBackground();
                        menu.draw();
                        SDL_Flip(screen);
                        SDL_Delay(250);
                        done = true;
                    }
                }
                break;
            }
            case SDLK_DOWN   : {
                if ( !keyCatch ) {
                    menu.increment();
                }
                break;
            }
            case SDLK_UP: {
                if ( !keyCatch ) {
                    menu.decrement();
                }
                break;
            }
            default:
                break;
            }
            keyCatch = true;
        }
        if(event.type == SDL_KEYUP) {
            keyCatch = false;
            menu.lightOff();
        }
    }
    delete manager;
}
