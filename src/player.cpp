#include <iostream>
#include "player.h"

Player::Player():
    right("right"),
    left("left"),
    jump_left("jump_left"),
    jump_right("jump_right"),
    pause_left("pause_left"),
    pause_right("pause_right"),
    current(&pause_right)
{
    //std::cout << "left :" << &left <<std::endl;
    //std::cout << "right :" << &right <<std::endl;
}

Player::Player(const Player& t):
    right("right"),
    left("left"),
    jump_left("jump_left"),
    jump_right("jump_right"),
    pause_left("pause_left"),
    pause_right("pause_right"),
    current(t.getcurrent())
{}

Player& Player::operator = (const Player& t)
{
    current = t.getcurrent();
    return *this;
}


MultiSprite * Player::getleft()
{
    return &left;
}

MultiSprite * Player::getright()
{
    return &right;
}

MultiSprite * Player::getjumpLeft()
{
    return &jump_left;
}

MultiSprite * Player::getjumpRight()
{
    return &jump_right;
}

MultiSprite * Player::getpauseLeft()
{
    return &pause_left;
}

MultiSprite * Player::getpauseRight()
{
    return &pause_right;
}

void Player::stop()
{
    current->setVelocitytoZero();
}

void Player::setSpeed(float x, float y)
{
    current->setVelocity(x, y);
}

MultiSprite * Player::getcurrent() const
{
    return current;
}

void Player::setcurrent(MultiSprite * tmp)
{
    current = tmp;
}

bool Player::onGround() {
    Vector2f pos = current->getPosition();
    Vector2f vel = current->getVelocity();
    if ( pos[1] > 330) {
	pos[1] = 330;
	if (vel[0] >= 0) {
	    current = &right;
	    current->setPosition(pos);
	}
	else if (vel[0] < 0) {
	    current = &left;
	    current->setPosition(pos);
	}
	return true;
    }
    else {
	return false;
    }
}
