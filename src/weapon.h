#include <iostream>
#include "multisprite.h"
#include "gamedata.h"

class Weapon : public MultiSprite {
    public:
	explicit Weapon(const string &name, 
			const Vector2f &pos, const Vector2f vel,
			const std::vector<Frame*> &fm):
	    MultiSprite(name, pos, vel, fm),
	    distance(0),
	    maxDistance(Gamedata::getInstance().getXmlInt(name+"Distance")),
	    tooFar(false)
    {}
	virtual void update(Uint32 ticks);
	bool goneTooFar() const {return tooFar;}
	void reset() {
	    tooFar = false;
	    distance = 0;
	}

    private:
	float distance;
	float maxDistance;
	bool tooFar;
};

