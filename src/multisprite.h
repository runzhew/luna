#ifndef MULTISPRITE__H
#define MULTISPRITE__H

#include <string>
#include <iostream>
#include <vector>
#include "collisionStrategy.h"

using std::string;

#include "drawable.h"

#define LEFT  0
#define RIGHT 1
#define JUMP  2 
#define PAUSE 4
#define ENEMY 6

class MultiSprite : public Drawable
{
public:
    MultiSprite(const string&);

    MultiSprite(const std::string&, const Vector2f& pos, const Vector2f& vel,
                const std::vector<Frame*>& fms);

    MultiSprite(const MultiSprite&);
    MultiSprite& operator = (const MultiSprite &);
    virtual ~MultiSprite() { }

    virtual void draw() const;
    virtual void update(Uint32 ticks);
    virtual const Frame* getFrame() const {
        return frames[currentFrame];
    }
    int getworldWidth() const;
    int getframeWidth() const;
    void setStatus(int s);
    bool collidedWith(const MultiSprite *obj);

protected:
    const std::vector<Frame *> frames;
    int worldWidth;
    int worldHeight;

    float dt;
    unsigned currentFrame;
    unsigned numberOfFrames;
    unsigned frameInterval;
    float timeSinceLastFrame;
    int frameWidth;
    int frameHeight;
    int STATUS;
    CollisionStrategy * strategy;

    void advanceFrame(Uint32 ticks);
};
#endif
